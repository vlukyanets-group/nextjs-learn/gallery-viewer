import fs from "fs";
import {NextRequest} from "next/server";

export async function GET(request: NextRequest, context: any) {
  try {
    const content = fs.readFileSync(`D:/images/${context.params.image_id}.jpg`);
    const headers = new Headers();
    headers.set("Content-Type", "image/jpeg");
    return new Response(content, { status: 200, headers});
  } catch (err) {
    console.error(err);
    return new Response(null, { status: 404 });
  }
}
