import {NextRequest, NextResponse} from "next/server";

function shuffleArray(a: string[]) {
  return a.sort(() => Math.random() - 0.5);
}

export async function GET(request: NextRequest) {
  const folder: string = "D:/images";

  try {
    const files = (await (await fetch(`${process.env.NEXT_PUBLIC_VERCEL_URL}/api/images`)).json()).images;
    shuffleArray(files);
    files.length = 3;
    return NextResponse.json({"images": files});
  } catch (err) {
    console.error(err);
    return new Response(null, { status: 500 });
  }
}
