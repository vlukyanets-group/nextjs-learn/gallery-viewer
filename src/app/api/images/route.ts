import fs from "fs";
import path from "path";
import {NextRequest, NextResponse} from "next/server";

export async function GET(request: NextRequest) {
  const folder: string = "D:/images";

  try {
    const files = fs.readdirSync(folder);
    const jpegFiles = files.filter((file) => path.extname(file).toLowerCase() === '.jpg').map(f => path.parse(f).name);
    return NextResponse.json({"images": jpegFiles});
  } catch (err) {
    console.error(err);
    return new Response(null, { status: 500 });
  }
}
