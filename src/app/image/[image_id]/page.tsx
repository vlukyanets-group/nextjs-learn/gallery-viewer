import Image from 'next/image'
import styles from './image.module.css'
import Link from "next/link";

export default async function ImagePage({ params }: { params: any }) {
  const currentImageId = params.image_id;
  const seeMoreImageIds = (await (await fetch(`${process.env.NEXT_PUBLIC_VERCEL_URL}/api/images/random`)).json()).images as string[];

  const title = (await (await fetch(`${process.env.NEXT_PUBLIC_VERCEL_URL}/api/images/titles/${currentImageId}`)).json()).title as string;

  return (
    <>
      <Link className={styles.go_home} href={`/`}>Home</Link>
      <main className={styles.main}>
        <h1 className={styles.h1}>{title}</h1>
        <div className={styles.main_box} style={{width: 700, height: 700}}>
          <Image src={`/api/images/${currentImageId}`} width={700} height={700} alt=""/>
        </div>
        <h2 className={styles.h2}>See More Shit</h2>
        <div className={styles.image_container}>
          {seeMoreImageIds.map((imageId: string, index) => (
            <Link key={index} href={`/image/${imageId}`}>
              <div className={styles.image_box} style={{width: 350, height: 350}}>
                <Image className={styles.image} src={`/api/images/${imageId}`} width={350} height={350} alt=""/>
              </div>
            </Link>
          ))}
        </div>
      </main>
    </>
  )
}

export const revalidate = 0;
