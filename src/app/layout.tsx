import './globals.css'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Gallery Viewer',
  description: '',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        {children}
        <h3 className="copyright">
          Developed by&nbsp;
          <a href="https://github.com/vlukyanets" target="_blank" className="rainbow_text">Valentin Lukyanets</a>
          &nbsp;&&nbsp;
          <a href="https://github.com/Yarodash" target="_blank" className="rainbow_text">Yaroslav Lukyanets</a>
          <br/>
          <p className="gray">© 202X-Dont-remember</p>
        </h3>
      </body>
    </html>
  )
}
