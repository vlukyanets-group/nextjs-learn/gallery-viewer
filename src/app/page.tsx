import Image from 'next/image'
import styles from './page.module.css'
import Link from "next/link";

function shuffleArray(a: string[]) {
  return a.sort(() => Math.random() - 0.5);
}

export default async function Home() {
  const jsonResponse = await (await fetch(`${process.env.NEXT_PUBLIC_VERCEL_URL}/api/images`)).json();
  const items = jsonResponse.images;
  const imageIds = shuffleArray(items);

  const titles: { [key: string]: string } = {};

  for (const item of items) {
    titles[item] = (await (await fetch(`${process.env.NEXT_PUBLIC_VERCEL_URL}/api/images/titles/${item}`)).json()).title as string;
  }

  return (
    <main className={styles.main}>
      <h1 className={styles.h1}>The world best pictures 2023</h1>
      <div className={styles.image_container}>
        {imageIds.map((imageId: string, index) => (
          <Link key={index} href={`/image/${imageId}`}>
            <div className={styles.image_box} style={{width: 500, height: 500}}>
              <Image className={styles.image} src={`/api/images/${imageId}`} width={500} height={500} alt=""/>
              <p className={styles.name}>{titles[imageId]}</p>
            </div>
          </Link>
        ))}
      </div>
    </main>
  )
}
